'use strict';

async function getPeopleTweets(URL) {
    try {
        // * selección botones
        const sendButton = document.querySelector('form > button');
        const tweetContainer = document.querySelector('#tweets');
        // console.log(tweetContainer);

        // * handle del botón tweet
        const handleTweetButton = async (e) => {
            e.preventDefault();

            const response = await fetch('https://randomuser.me/api/?results');
            const data = await response.json();

            // * accediendo al user de la api
            const [user] = data.results;
            console.log(user);
            const fullName = `${user.name.first} ${user.name.last}`;
            const userImage = user.picture.large;

            const localTime = new Date().toLocaleDateString('es-ES', {
                hour: '2-digit',
                minute: '2-digit',
                second: '2-digit',
                day: 'numeric',
                month: 'numeric',
                year: 'numeric',
            });
            // console.log(localTime);

            const tweetInput = e.target.previousElementSibling;

            // * Condiciones no value && límite caracteres
            if (tweetInput.value !== '' && tweetInput.value.length < 101) {
                const newTweet = document.createElement('li');
                tweetContainer.insertBefore(
                    newTweet,
                    tweetContainer.firstChild
                );
                newTweet.innerHTML = `
                    <img class="avatar" src="${userImage}" alt="">
                    <div class="userBody"><p class="fullName">${fullName}</p>
                    <p class="tweetContent">${tweetInput.value}</p></div>
                    <footer>
                        <time>${localTime}</time>
                    </footer>                    
                    <button class="borrar">Borrar</button>           
                    `;
                // console.log(newTweet);
                tweetInput.value = '';
                tweetInput.focus();
            } else {
                alert(
                    '¡No has escrito nada! (O has ingresado más de 100 caracteres)'
                );
            }
        };

        // * handle del delete
        const handleDeleteButton = (e) => {
            const { target } = e;

            if (target.matches('button.borrar')) {
                const insertedTweet = target.parentElement;
                insertedTweet.remove();
            }
        };

        sendButton.addEventListener('click', handleTweetButton);
        tweetContainer.addEventListener('click', handleDeleteButton);
    } catch (error) {
        return error;
    }
}

getPeopleTweets();
